
const express = require('express');
const router = express.Router();
const redis = require('redis');
const config = require('../constants/config');


const configs = {
    host: 'localhost',
    port: 6379,
}

const client = redis.createClient(configs);

client.on("connect", function(error) {
    console.log("Redis connected!!");
})

module.exports = client
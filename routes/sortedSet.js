const express = require('express');
const router = express.Router();
const config = require('../constants/config');
const cli = require('../constants/redisClient');
const _=require('underscore');

//1-10
cli.zadd("myzset", 5.99940, "1-medis-lk");
cli.zadd("myzset", 4.99950, "2-amaesh-lk");
cli.zadd("myzset", 1.99960, "3-gunawardana-lk");
cli.zadd("myzset", 3.99970, "4-jayawarman-in");
cli.zadd("myzset", 2.99980, "5-rajiv-in");
cli.zadd("myzset", 5.99945, "6-malith-lk");
cli.zadd("myzset", 5.99948, "7-sony-in");
cli.zadd("myzset", 4.99943, "8-munasinghe-lk");
cli.zadd("myzset", 3.99942, "9-john-en");
cli.zadd("myzset", 6.99949, "10-benadict-en");


// //11-20
// cli.zadd("myzset", 6.99955, "11");
// cli.zadd("myzset", 4.99958, "12");
// cli.zadd("myzset", 6.99990, "13");
// cli.zadd("myzset", 3.99951, "14");
// cli.zadd("myzset", 4.99961, "15");
// cli.zadd("myzset", 3.99966, "16");
// cli.zadd("myzset", 6.99975, "17");
// cli.zadd("myzset", 6.99978, "18");
// cli.zadd("myzset", 1.99971, "19");
// cli.zadd("myzset", 4.99985, "20");


// // //21-30
// cli.zadd("myzset", 2.99988, "21");
// cli.zadd("myzset", 5.99982, "22");
// cli.zadd("myzset", 6.99987, "23");
// cli.zadd("myzset", 3.99973, "24");
// cli.zadd("myzset", 4.99977, "25");
// cli.zadd("myzset", 5.99988, "26");
// cli.zadd("myzset", 4.99966, "27");
// cli.zadd("myzset", 2.99955, "28");
// cli.zadd("myzset", 6.99944, "29");
// cli.zadd("myzset", 5.99989, "30");


// // //31-40
// //same previous times with different scores
// cli.zadd("myzset", 3.99940, "31");
// cli.zadd("myzset", 6.99950, "32");
// cli.zadd("myzset", 2.99960, "33");
// cli.zadd("myzset", 5.99970, "34");
// cli.zadd("myzset", 3.99980, "35");
// cli.zadd("myzset", 3.99945, "36");
// cli.zadd("myzset", 6.99948, "37");
// cli.zadd("myzset", 5.99943, "38");
// cli.zadd("myzset", 1.99942, "39");
// cli.zadd("myzset", 4.99949, "40");



// // //41-50
// cli.zadd("myzset", 41, "4.99955");
// cli.zadd("myzset", 42, "5.99958");
// cli.zadd("myzset", 43, "5.99990");
// cli.zadd("myzset", 44, "5.99951");
// cli.zadd("myzset", 45, "6.99961");
// cli.zadd("myzset", 46, "5.99966");
// cli.zadd("myzset", 47, "4.99975");
// cli.zadd("myzset", 48, "3.99978");
// cli.zadd("myzset", 49, "5.99971");
// cli.zadd("myzset", 50, "6.99985");


// // //51-60
// cli.zadd("myzset", 51, "4.99988");
// cli.zadd("myzset", 52, "2.99982");
// cli.zadd("myzset", 53, "5.99987");
// cli.zadd("myzset", 54, "5.99973");
// cli.zadd("myzset", 55, "3.99977");
// cli.zadd("myzset", 56, "4.99988");
// cli.zadd("myzset", 57, "6.99966");
// cli.zadd("myzset", 58, "1.99955");
// cli.zadd("myzset", 59, "5.99944");
// cli.zadd("myzset", 60, "4.99989");

// // //61-70
// //time takes more that 1 min
// cli.zadd("myzset", 61, "5.99930");
// cli.zadd("myzset", 62, "4.99920");
// cli.zadd("myzset", 63, "6.99910");
// cli.zadd("myzset", 64, "4.99935");
// cli.zadd("myzset", 65, "3.99937");
// cli.zadd("myzset", 66, "6.99934");
// cli.zadd("myzset", 67, "5.99925");
// cli.zadd("myzset", 68, "6.99922");
// cli.zadd("myzset", 69, "4.99928");
// cli.zadd("myzset", 70, "5.99926");


// // //71-80
// cli.zadd("myzset", 71, "4.99915");
// cli.zadd("myzset", 72, "2.99918");
// cli.zadd("myzset", 73, "1.99913");
// cli.zadd("myzset", 74, "5.99906");
// cli.zadd("myzset", 75, "6.99908");
// cli.zadd("myzset", 76, "4.99909");
// cli.zadd("myzset", 77, "5.99938");
// cli.zadd("myzset", 78, "4.99921");
// cli.zadd("myzset", 79, "5.99912");
// cli.zadd("myzset", 80, "3.99939");

// // //81-90
// //time takes more that 1 min with different scores
// cli.zadd("myzset", 81, "4.99930");
// cli.zadd("myzset", 82, "5.99920");
// cli.zadd("myzset", 83, "5.99910");
// cli.zadd("myzset", 84, "3.99935");
// cli.zadd("myzset", 85, "5.99937");
// cli.zadd("myzset", 86, "4.99934");
// cli.zadd("myzset", 87, "6.99925");
// cli.zadd("myzset", 88, "4.99922");
// cli.zadd("myzset", 89, "3.99928");
// cli.zadd("myzset", 90, "4.99926");


// // //91-100
// cli.zadd("myzset", 91, "5.99915");
// cli.zadd("myzset", 92, "4.99918");
// cli.zadd("myzset", 93, "5.99913");
// cli.zadd("myzset", 94, "4.99906");
// cli.zadd("myzset", 95, "5.99908");
// cli.zadd("myzset", 96, "5.99909");
// cli.zadd("myzset", 97, "4.99938");
// cli.zadd("myzset", 98, "5.99921");
// cli.zadd("myzset", 99, "5.99921");
// cli.zadd("myzset", 100, "5.99921");

// cli.zincrby("myzset", 5, "juis");


router.get('/', async(req, res)=>{
    try {
        
        cli.zrevrange("myzset", 0, 10, 'withscores', function (err, list) {

            var lists=_.groupBy(list, function(a,b) {
                return Math.floor(b/2);
            });
            console.log( _.toArray(lists) );
        });
        


    } catch (error) {
        console.log(error);
    return res.send({
      success: false,
      message: "Server Error",
    });
    }
})


cli.zrevrange("myzset", 0, 10, 'withscores', function (err, list) {

    var lists=_.groupBy(list, function(a,b) {
        return Math.floor(b/2);
    });
    console.log( _.toArray(lists) );
});








module.exports = router

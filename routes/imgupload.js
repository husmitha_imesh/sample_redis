const express = require("express");
const router = express.Router();

const { Storage } = require("@google-cloud/storage");
const stream = require("stream");
var gcsConfig = require("../gcsConfig");



//google cloud storage
const GOOGLE_CLOUD_PROJECT_ID = gcsConfig.GOOGLE_CLOUD_PROJECT_ID;
const GOOGLE_CLOUD_KEYFILE = gcsConfig.STORAGE_JSON;
const BUCKET_NAME = gcsConfig.BUCKET_NAME;
const IMAGE_FOLDER = "dev";
const storage = new Storage({
  projectId: GOOGLE_CLOUD_PROJECT_ID,
  keyFilename: GOOGLE_CLOUD_KEYFILE,
});


//img upload fn
const ImageUploadFn = async (base64, folder, id, name) => {
    return new Promise((resolve, reject) => {
      let bufferStream = new stream.PassThrough();
      bufferStream.end(
        Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), "base64")
      );
      let myBucket = storage.bucket(BUCKET_NAME);
      let file = myBucket.file(`${IMAGE_FOLDER}/${folder}/${id}/${name}.jpg`);
      bufferStream
        .pipe(
          file.createWriteStream({
            metadata: {
              contentType: "image/jpeg",
              metadata: {
                custom: "metadata",
              },
            },
            public: true,
            validation: "md5",
          })
        )
        .on("error", function (err) {
          reject("It broke", err);
        })
        .on("finish", function () {
          resolve(
            `https://storage.googleapis.com/kramp-bucket/${IMAGE_FOLDER}/${folder}/${id}/${name}.jpg`
          );
        });
    });
  };


  router.post('/add', async(req, res) =>{

    const {imageURL} = req.body.imageURL

    user = new user({
        imageURL : "req.body.imageURL"
    })


    let tmpImageURL = await ImageUploadFn(
        imageURL,
        "prizes",
        masterPrize._id,
        "Profile"
      );
      user.imageURL = tmpImageURL;
      await user.save();

      res.send({
          success: true,
          message: "success",

      })

  })


  module.exports = router
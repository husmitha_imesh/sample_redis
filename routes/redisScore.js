const express = require("express");
const router = express.Router();
const RedisClient = require("../constants/redisClient");

router.get("/test", async (req, res) => {
  try {
    return res.send({
      success: false,
      message: "Working",
    });
  } catch (error) {
    return res.send({
      success: false,
      message: "Server Error",
    });
  }
});

router.get("/score/:name/:score", async (req, res) => {
  try {
    let data = await AddValue(
      "test",
      "score",
      req.params.score,
      req.params.name
    );
    res.send({
      success: true,
      message: "Data Added",
      content: data,
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: true,
      message: "Server Error",
    });
  }
});

router.get("/all", async (req, res) => {
  try {
    let data = await GetRankRange("test", "score", 0, 10);
    res.send({
      success: true,
      message: "Get First 10",
      content: data,
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server Error",
    });
  }
});

router.get("/getSingle/:name", async (req, res) => {
  try {
    let data = await GetRank("test", "score", "sen2");
    res.send({
      success: true,
      message: "Get Rank",
      content: data,
    });
  } catch (error) {
    console.log(error);
    return res.send({
      success: false,
      message: "Server Error",
    });
  }
});

const AddValue = async (DBName, CollectionName, value, key) => {
  return new Promise((resolve, reject) => {
    RedisClient.zadd(
      `${DBName}:${CollectionName}`,
      value,
      key,
      function (err, value) {
        if (value > 0) {
          resolve(value);
        } else {
          reject("Duplicate Value");
        }
      }
    );
  });
};

const GetRank = async (DBName, CollectionName, key) => {
  return new Promise((resolve, reject) => {
    RedisClient.zrank(`${DBName}:${CollectionName}`, key, (err, value) => {
      if (value) {
        resolve(value);
      } else {
        reject("Data Not Exist");
      }
    });
  });
};

const GetRankRange = async (DBName, CollectionName, startRank, endRank) => {
  return new Promise((resolve, reject) => {
    RedisClient.zrevrange(
      `${DBName}:${CollectionName}`,
      startRank,
      endRank,
      "withscores",
      function (err, list) {
        if (list) {
          resolve(
            list.chunk_inefficient(2).map((x) => {
              return {
                name: x[0],
                score: x[1],
              };
            })
          );
        }
        if (err) {
          reject(err);
        }
      }
    );
  });
};

Object.defineProperty(Array.prototype, "chunk_inefficient", {
  value: function (chunkSize) {
    var array = this;
    return [].concat.apply(
      [],
      array.map(function (elem, i) {
        return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
      })
    );
  },
});

module.exports = router;

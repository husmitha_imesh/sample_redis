const express = require('express');
const router = express.Router();
const config = require('../constants/config');
const User = require('../models/user');
const cli = require('../constants/redisClient');

// const redis = require('redis');

// const configs = {
//     host: 'localhost',
//     port: 6379,
// }

// const client = redis.createClient(configs);

// client.on("connect", function(error) {
//     console.log("Redis connected!!");
// })


router.post('/add', async(req, res) => {
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
    })

    await user.save();

    cli.hmset('users', [
        'Fname', req.body.name,
        'email', req.body.email,
        'address', req.body.address,

    ], function(err, reply){
        if(err){
            console.log(err)
        }
        console.log(reply)
    })


    // client.hgetall('users', function(err, obj){
    //     console.log(obj)
    // })


    res.send({
        success: true,
        message: "Successfully added",
      });
})


router.get('/getAll', async(req, res)=>{
    
    cli.hgetall('users', function(err, obj){
        console.log(obj)
    });

    res.send({
        message: "success",
    })
})



module.exports = router
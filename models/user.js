const mongoose  = require('mongoose')

const userSchema = mongoose.Schema({
   name:  { type: String, required: true, unique: false },
   email:  { type: String, required: true, unique: false },
   address:  { type: String, required: true, unique: false },
   imgageURL:  { type: String, required: false, unique: false }
});

module.exports = mongoose.model("Users", userSchema);
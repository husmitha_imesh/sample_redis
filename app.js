const express = require("express");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
// const cli = require('./constants/redisClient');

const cors = require("cors");


const app = express();

// app.use(morgan("dev"));
app.use(cors());
app.use(bodyparser.json({ limit: "50mb" }));
app.use(
  bodyparser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 1000000,
  })
);

app.use(bodyparser.urlencoded({ extended: true }));

app.use(bodyparser.json());

app.use(express.static("public"));

/*
 * Import all routes files
 */
const config = require("./constants/config");
// const lader = require("./routes/leaderboard");
// const user = require("./routes/user");
// const sortedset = require("./routes/sortedSet");
const RedisScores = require("./routes/redisScore");




// app.use("/api/leader", lader);
// app.use("/api/user", user);
// app.use("/api/sortedset", sortedset);
app.use("/api/scores", RedisScores);



mongoose.connect(
  config.Db,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function () {
    console.log("Db Connected!");
    // console.log(config);
  }
);

app.listen(config.port);
